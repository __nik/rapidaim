# rapidaim
## Rapid Aim test answers


### Histogram
To run the program
```sh
node histogram.js
```
> To change the histogram, change `:1: const histogram`

### Age Counting
To run the program
```sh
npm install
node app.js
```
> To change the age, change `:3: const age`

### Broken Stack
```sh
node brokenStack.js
```
> The stack won't hold states between function calls because the requested code isn't class based. For a class based representation, look at `brokenStackClass.js`

```sh
node brokenStackClass.js
```

### Vue Toggle Button
For assessment, check `/toggleButtonIndex.vue`

For a working demo, 
```sh
cd toggle-button
yarn install
yarn dev
```