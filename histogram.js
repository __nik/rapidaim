const histogram = [2,1,3,4,1];

/**
 * 
 * @param histogram Array of Y-axis values.
 * @return Area of the largest rectangle under the graph
 */
var largestRectangle = function (histogram) {
    var max = 0, rows = [];
    for (var i = 0; i < histogram.length; ++i) {
        var height = histogram[i];
        // Uniformly increment row lengths
        for (var j = 0; j < height; ++j) {
            if (!rows[j]) {
                rows[j] = 0;
            }
            ++rows[j]; // Increase length of current row segment
            max = Math.max(max, rows[j] * (j + 1));
        }
        for (var j = rows.length - 1; j > height - 1; --j){
            rows[j] = 0;
        }
    }
    return max;
};


var result = largestRectangle(histogram);
console.log("Histogram:", histogram, "Largest rectangle area:", result);