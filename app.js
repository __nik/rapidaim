const fetch = require('node-fetch');
const url = 'https://www.dropbox.com/s/ecfe8st296hztxf/age-counting.json?dl=1';
const age = 50;

/**
 *  1.  Get from API
 *  2.  Calculate age >= 50
 * 
 * 
 */
function calculateAge(){
    console.log('Retrieving data from API');

    // 1. GET data from API
    fetch(url)
    .then(res => res.json())
    .then(json => {
        console.log('Data retrieved');
        console.log(`Counting age >= ${age}`);

        console.log(json.data.match(/age(\s+)?=\d+/g).filter(x=>{return Number(x.replace(/[^\d]/g, '')) >= age}).length);
    });
}

calculateAge();