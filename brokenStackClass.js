class Stack {
	storage = [
		1,
		'{id: 1,value: "obj"}',
		"stringHolder",
		46
	];

	push = (element) => {
		this.storage.push(element);
		return this.storage;
	};
	pop = () => this.storage.pop();
	isempty = () => this.count() === 0;
	empty = () => (this.count() = 0);
	count = () => {
		return this.storage.length;
	};
	peek = () => {
		return this.storage[this.count() - 1];
	};
	swap = () => {
		if (this.count() > 1) {
			[this.storage[this.count() - 1], this.storage[this.count() - 2]] = [this.storage[this.count() - 2], this.storage[this.count() - 1]]
		}
		return this.storage;
	}
}
const stack = new Stack();

console.log(`Stack: ${stack.storage}`);
console.log(`Push (90): ${stack.push(90)}`);
console.log(`Count: ${stack.count()}`);
console.log(`Peek: ${stack.peek()}`);
console.log(`Swap: ${stack.swap()}`);
console.log(`Pop: ${stack.pop()}`);