function stack(stackOperation, stackValue) {
	var stackHolder = {
		// count: 3,
		count: 4,
		storage: [
			1,
			'{id: 1,value: "obj"}',
			"stringHolder",
			46
		]
	};

	var push = function (value) {
		// stackHolder.storage[stackHolder.count] = value;
		stackHolder.storage.push(value);
		// 
		stackHolder.count++;
		return stackHolder.storage;
	}

	var pop = function () {
		if (stackHolder.count === 0) {
			return [];
		}

		// var poppedItem = stackHolder.storage[stackHolder.count];
		var poppedItem = stackHolder.storage[stackHolder.count-1];
		// delete stackHolder.storage[stackHolder.count];
		delete stackHolder.storage[stackHolder.count-1];
		stackHolder.count--;
		return poppedItem;
	}

	var peek = function () {
		// return [stackHolder.storage[0]];
		return [stackHolder.storage[stackHolder.count - 1]];
	}

	var swap = function () {
		if (stackHolder.count > 1) {
			[stackHolder.storage[stackHolder.count - 1], stackHolder.storage[stackHolder.count - 2]] = [stackHolder.storage[stackHolder.count - 2], stackHolder.storage[stackHolder.count - 1]]
		}
		return stackHolder.storage;
	}

	switch (stackOperation) {
		case 'push':
			return push(stackValue);
			// break;
		case 'pop':
			return pop();
			// break;
		case 'swap':
			return swap();
			// break;
		case 'peek':
			return peek();
			// break;
		default:
			return stackHolder.storage;
	}
}

console.log(`Push: ${stack('push', 99)}`);
console.log(`Pop: ${stack('pop', false)}`);
console.log(`Swap: ${stack('swap', false)}`);
console.log(`Peek: ${stack('peek', false)}`);